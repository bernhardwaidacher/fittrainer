import { FittrainerAngularPage } from './app.po';

describe('fittrainer-angular App', function() {
  let page: FittrainerAngularPage;

  beforeEach(() => {
    page = new FittrainerAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
